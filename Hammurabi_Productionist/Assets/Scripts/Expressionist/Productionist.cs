﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Linq;

public class Productionist
{
	[JsonProperty(PropertyName = "markups")]
	public Dictionary<string, string[]> MarkupSets;
	[JsonProperty(PropertyName = "nonterminals")]
	public Dictionary<string, NonTerminalSymbol> NonTerminalSymbols;
	[JsonProperty(PropertyName = "system_vars")]
	public string[] SystemVariables;

	private NonTerminalSymbol _root;
	public NonTerminalSymbol Root
	{
		get
		{
			if(_root == null)
			{
				foreach (NonTerminalSymbol nonTerminalSymbol in NonTerminalSymbols.Values)
				{
					string[] structureTags = new string[1];
					nonTerminalSymbol.markups.TryGetValue("Structure", out structureTags);
					if (structureTags != null)
					{
						foreach (string tag in structureTags)
						{
							if (tag.ToLower() == "root")
							{
								_root = nonTerminalSymbol;
							}
						}
					}
				}
			}
			return _root;
		}
	}

	public string Output { get; set; }
	public string OutputFlags { get; set; }

	public Productionist(string grammar)
	{
		string json = System.IO.File.ReadAllText(@Application.dataPath + "/Grammars/" + grammar + ".json");
		JsonConvert.PopulateObject(json, this);

		foreach (NonTerminalSymbol nonTerminalSymbol in NonTerminalSymbols.Values)
		{
			parseNonTerminals(nonTerminalSymbol);
		}
	}

	public Expression GenerateExpression(string markup = "")
	{
		DialogManager.instance.ActiveProductionist = this;

		List<string> parsedMarkup = ParseMarkup(markup);

		bool good = false;
		int attempts = 0;

		while(!good && attempts < 100)
		{
			Output = "";
			OutputFlags = "";

			Output = Root.expand(parsedMarkup);

			if (!Output.Contains(DialogManager.kImpossibleExpression) || Input.GetKeyDown("space"))
			{
				//Debug.Log ("not impossible, go!");
				good = true;
			}

			attempts++;
		}

		if (Output.Contains (DialogManager.kImpossibleExpression)) {
			Output = "...";
			OutputFlags = OutputFlags+ (";pragma_silence");
		}
		//Output = !Output.Contains(DialogManager.kImpossibleExpression) ? Output : "...";


		return new Expression(Output, markup.Split(';'), OutputFlags.Split(';'));
	}

	private void parseNonTerminals(NonTerminalSymbol nonTerminalSymbol)
	{
		foreach (Rule rule in nonTerminalSymbol.rules)
		{
			parseRule(rule);
		}
	}

	private void parseRule(Rule rule)
	{
		foreach (string word in rule.expansion)
		{
			parseExpansion(word, rule);
		}
	}

	private void parseExpansion(string word, Rule rule)
	{
		if (word.Contains("[["))
		{
			char[] t = { '[', ']' };
			word = word.Trim(t);
			NonTerminalSymbol test = new NonTerminalSymbol();
			NonTerminalSymbols.TryGetValue(word, out test);
			rule.newExpansion.Add(test);
			test.Id = word;
		}
		else
		{
			rule.newExpansion.Add(word);
		}
	}

	private static IEnumerable<string> SplitAndKeep(string s, char[] delims)
	{
		int start = 0, index;

		while ((index = s.IndexOfAny(delims, start)) != -1)
		{
			if (index - start > 0)
				yield return s.Substring(start, index - start);
			yield return s.Substring(index, 1);
			start = index + 1;
		}

		if (start < s.Length)
		{
			yield return s.Substring(start);
		}
	}

	private List<string> ParseMarkup(string markup)
	{
		List<string> tempMarkup = markup.Split(';').ToList();
		return tempMarkup;
	}
}
