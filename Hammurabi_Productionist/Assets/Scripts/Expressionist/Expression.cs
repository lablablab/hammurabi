﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Expression {

	public string Line;
	public string[] Markup;
	public string[] Flags;

	public Expression(string line, string[] markup, string[] flags)
	{
		Line = line;
		Markup = markup;
		Flags = flags;
	}

	public bool ContainsFlag(string flag)
	{
		for(int i = 0; i < Flags.Length; i++)
		{
			if(Flags[i] == flag)
			{
				return true;
			}
		}

		return false;
	}

	public override string ToString()
	{
		return String.Format("{0}", Line);
	}
}
