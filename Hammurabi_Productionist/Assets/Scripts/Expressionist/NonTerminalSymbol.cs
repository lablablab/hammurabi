﻿ using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class NonTerminalSymbol{
	[JsonProperty(PropertyName="complete")]
	public bool complete;
	[JsonProperty(PropertyName="deep")]
	public bool deep;
	[JsonProperty(PropertyName="markup")]
	public Dictionary<string, string[]> markups;
	[JsonProperty(PropertyName="rules")]
	public List<Rule> rules;
	
	public string Id;
	
	public string expand(List<string> parsedMarkup)
	{
		Dictionary<Rule, int> pool = new Dictionary<Rule, int>();
		
		foreach (Rule rule in rules)
		{
			bool badRule = false;
			int rankScore = 0;
			for (int i = 0; i < rule.newExpansion.Count; i++)
			{
				NonTerminalSymbol NonTerminal = rule.newExpansion[i] as NonTerminalSymbol;
				if(NonTerminal == null)
				{
					continue;
				}

				// start testing the NonTerminalSymbol tags against the markup to validate the rule for consideration 

				foreach (string tagset in NonTerminal.markups.Keys)
				{
					if (NonTerminal.markups.Count == 0 || !NonTerminal.markups.ContainsKey(tagset))
					{
						continue;
					}
					
					foreach (string tag in NonTerminal.markups[tagset])
					{
						// MustNotHave behaviour : NonTerminalSymbol tag MUST NOT appear in the markup for the rule to be considered (yet to be tested)
						if(DialogManager.instance.MustNotHave.Contains(tagset))
						{

							if(parsedMarkup.Contains(tag))
							{
								//Debug.Log ("Must have disqualified: "+rule.expansion[0]+ "with tag: "+tag);
								badRule = true;
								break;
							}

							break;
						}						
						// MustHave behaviour : NonTerminalSymbol tag MUST appear in the markup for the rule to be considered (yet to be tested)
						if(DialogManager.instance.MustHave.Contains(tagset))
						{

							if(!parsedMarkup.Contains(tag))
							{
								//Debug.Log ("Must have disqualified: "+rule.expansion[0]+ "with tag: "+tag);
								badRule = true;
								break;
							}
							else{

								rankScore+=5;
							}
							break;
						}

						// MustNotNotHave behaviour : NonTerminalSymbol tag MUST NOT contradict a tag from the same sub-tagset that appears in the markup for the rule to be considered
						if (DialogManager.instance.MustNotNotHave.Contains(tagset))
						{
							if (!parsedMarkup.Contains(tag))
							{
								string tagPrefix = tag.Split('_')[0];
								foreach (string str in parsedMarkup)
								{
									if (str.Contains(tagPrefix))
									{
										badRule = true;
										break;
									}
								}
								break;
							}
							break;
						}

						// NiceToHave behaviour : The more NonTerminalSymbol tag that fit the markup, the more chance this rule has to be considered
						if (DialogManager.instance.NiveToHave.Contains(tagset))
						{
							if (parsedMarkup.Contains(tag))
							{
								rankScore++;
							}
							break;
						}
					}
				}
				
				if (badRule)
				{
					break;
				}
			}
			if (badRule)
			{
				continue;
			}

			pool.Add(rule, rankScore);
		}

		Rule ruleToBeFired = null;
		string expandedSymbol = "";

		if (pool.Count != 0)
		{
			List<Rule> actualPool = new List<Rule>();
			int highestScore = 0;

			foreach(Rule rule in pool.Keys)
			{
				if(pool[rule] > highestScore)
				{
					//actualPool.Clear();
					actualPool.Add(rule);
					highestScore = pool[rule];
				}
				else if (pool[rule] == highestScore)
				{
					actualPool.Add(rule);
				}
				else if (pool[rule] < highestScore)
				{
					actualPool.Insert(actualPool.Count,rule);
				}
			}
			//Debug.Log("Nb of rules in pool: "+actualPool.Count+" and highest score: "+highestScore);
			if (highestScore == 0) {
				ruleToBeFired = actualPool [Random.Range (0, actualPool.Count)];
			} else {
				ruleToBeFired = actualPool [Random.Range (0, actualPool.Count)/2];
			}
		}

		if(ruleToBeFired != null)
		{
			expandedSymbol = ruleToBeFired.Fire(parsedMarkup);
		}
		
		if (!expandedSymbol.Equals(""))
		{
			return expandedSymbol;
		}
		else
		{
			return DialogManager.kImpossibleExpression;
		}
	}
}

