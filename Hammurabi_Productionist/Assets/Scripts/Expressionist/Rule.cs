﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class Rule{
	[JsonProperty(PropertyName="app_rate")]
	public int appRate;
	[JsonProperty(PropertyName="expansion")]
	public List<string> expansion;
	public ArrayList newExpansion = new ArrayList();

	public string Fire(List<string> parsedMarkup)
	{
		string expandedSymbol = "";
		for(int i = 0; i < newExpansion.Count; i++)
		{
			if (expansion[i].Contains("[["))
			{
				foreach(string tagSet in (newExpansion[i] as NonTerminalSymbol).markups.Keys)
				{
					if(!DialogManager.instance.OutputFlags.Contains(tagSet))
					{
						continue;
					}

					if(DialogManager.instance.ActiveProductionist != null)
					{
						foreach (string tag in (newExpansion[i] as NonTerminalSymbol).markups[tagSet])
						{
							DialogManager.instance.ActiveProductionist.OutputFlags += (DialogManager.instance.ActiveProductionist.OutputFlags.Length != 0 ? ";" : "") + tag;
						}
					}
				}
				
				expandedSymbol += (newExpansion[i] as NonTerminalSymbol).expand(parsedMarkup);
			}
			else if(expansion[i].Contains("["))
			{
				string word = expansion[i];
				char[] t = { '[', ']' };
				word = word.Trim(t);

				string newWord = "";

				foreach (string markup in parsedMarkup)
				{
					if(markup.Contains(word))
					{
						newWord = markup.Split('_')[1];
					}
				}
				
				expandedSymbol += (newWord != "" ? newWord : (newExpansion[i] as string));
			}
			else
			{
				expandedSymbol += (newExpansion[i] as string);
			}
		}
		return expandedSymbol;
	}
}
