﻿using UnityEngine;
using System;
using System.Collections;

public class AgricultureState : OfficeState {

	private AgricultureBalance _balanceUnit = BalanceManager.instance.Agriculture;

	public int ProductionModifier {	get; private set; }
	public int Budget { get; private set; }
	public int Tempering { get; private set; }
	public int WorkableLand { get; private set; }
	public int SeededLand { get; private set; }
	public int RawHarvest { get; private set; }

	public AgricultureState() : base()
	{
		
	}

	public AgricultureState(AgricultureState state)
	{
		ProductionModifier = state.ProductionModifier;
		Budget = state.Budget;
		Tempering = state.Tempering;
		WorkableLand = state.WorkableLand;
		SeededLand = state.SeededLand;
		RawHarvest = state.RawHarvest;
	}
	
	public override void AdvanceState()
	{
		// Base variables fetching

		Advisor = GameState.CharacterState.AgricultureAdvisor;

		base.AdvanceState();

		ProductionModifier = UnityEngine.Random.Range(_balanceUnit.ProductionModifierLow, _balanceUnit.ProductionModifierHigh);

		Budget = int.Parse(TurnPanel.instance.AgricultureBudget.text);
		GameState.GlobalState.GrainStore -= Budget;

		// Base variables adjustments

		Tempering = 0;

		if (TemperingModifier == BudgetModifier.Negative)
		{
			Tempering = -(int)(Budget * _balanceUnit.StealPercentage);
		}
		else if (TemperingModifier == BudgetModifier.Positive)
		{
			Tempering = (int)(GameState.CharacterState.AgricultureAdvisor.Grain * _balanceUnit.GivePercentage);
		}

		Budget += Tempering;
		Advisor.Grain -= Tempering;

		if (CompetenceModifier == BudgetModifier.Negative)
		{
			ProductionModifier = Mathf.Max(ProductionModifier - _balanceUnit.CompetenceModifier, _balanceUnit.ProductionModifierLow);
		}
		else if (CompetenceModifier == BudgetModifier.Positive)
		{
			ProductionModifier = Mathf.Min(ProductionModifier + _balanceUnit.CompetenceModifier, _balanceUnit.ProductionModifierHigh);
		}
		
		// Additional variables computations

		WorkableLand = Math.Min(GameState.GlobalState.Acreage - int.Parse(TurnPanel.instance.CommerceBudget.text), GameState.GlobalState.Population * _balanceUnit.LandWorkedByPopulation);

		SeededLand = Math.Min(Budget, WorkableLand);

		RawHarvest = SeededLand * ProductionModifier;

		// Global variables adjustments

		GameState.GlobalState.GrainStore += RawHarvest;
	}

	public override string ToString()
	{
		return String.Format(
			"_Agriculture State_ ProductionModifier: {0}, Budget: {1}, Tempering: {2}{3}, WorkableLand: {4}, SeededLand: {5}, RawHarvest: {6}{7}{8}",
			ProductionModifier,
			Budget,
			TemperingModifier == BudgetModifier.Positive ? "+" : "",
			Tempering,
			WorkableLand,
			SeededLand,
			RawHarvest,
			CompetenceModifier == BudgetModifier.Positive ? " (Competence Bonus)" : "",
			CompetenceModifier == BudgetModifier.Negative ? " (Incompetence Malus)" : ""
		);
	}

}
