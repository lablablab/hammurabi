﻿using UnityEngine;
using System;
using System.Collections;

public class GlobalState : State
{
	private GlobalBalance _balanceUnit = BalanceManager.instance.Global;

	public int TotalLand { get; set; }

	public int Acreage;
	public int OtherLand;
	public int GrainStore;
	public int Population;
	public int Happiness;
	public int KingApproval;

	public int GrainLoss;
	public int UntouchedNetHarvest;
	public int NetHarvest;

	public GlobalState() : base()
	{
		Acreage = _balanceUnit.BaseAcreage;
		OtherLand = _balanceUnit.BaseOtherLand;
		GrainStore = _balanceUnit.BaseGrainStore;
		Population = _balanceUnit.BasePopulation;
		Happiness = _balanceUnit.BaseHappiness;
		KingApproval = _balanceUnit.BaseKingApproval;

		GrainLoss = 0;
		UntouchedNetHarvest = 0;
		NetHarvest = 0;
	}

	public GlobalState(GlobalState state)
	{
		TotalLand = state.TotalLand;
		Acreage = state.Acreage;
		OtherLand = state.OtherLand;
		GrainStore = state.GrainStore;
		Population = state.Population;
		Happiness = state.Happiness;
		KingApproval = state.KingApproval;

		GrainLoss = state.GrainLoss;
		UntouchedNetHarvest = state.UntouchedNetHarvest;
		NetHarvest = state.NetHarvest;
	}

	public override void AdvanceState()
	{
		int advisorLand = 0;
		foreach (Character advisor in GameState.CharacterState.Characters)
		{
			advisorLand += advisor.Land;
		}
		TotalLand = Acreage + OtherLand + advisorLand;

		// additional system variables computation

		int Stealing = 0; // Implement from grammar generating OutputFlags
		
		GrainLoss = (int)(GameState.CrisesState.ActiveCrisis != null && GameState.CrisesState.ActiveCrisis.Name == CrisesState.kLocusts ? 
			(1 - BalanceManager.instance.Crises.LocustGrainModifier) : GameState.RatsState.RatsPercent) * GameState.AgricultureState.RawHarvest;

		UntouchedNetHarvest = GameState.AgricultureState.RawHarvest - GrainLoss;
		
		NetHarvest = UntouchedNetHarvest - Stealing;
	}

	public override string ToString()
	{
		return String.Format(
			"_Global State_ TotalLand: {0}, Acreage: {1}, OtherLand: {2}, GrainStore: {3}, Population: {4}, KingApproval: {5}, GrainLoss: {6}, UntouchedNetHarvest: {7}, NetHarvest: {8}",
			TotalLand,
			Acreage,
			OtherLand,
			GrainStore,
			Population,
			KingApproval,
			GrainLoss,
			UntouchedNetHarvest,
			NetHarvest
		);
	}

}
