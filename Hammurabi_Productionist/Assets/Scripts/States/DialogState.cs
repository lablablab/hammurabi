﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Text;

public class DialogState : State {
	
	public enum ExpressionType
	{
		Introduction,
		Advice,
		Decision,
		Report
	}

	public Expression AgricultureReport { get; private set; }
	public Expression CommerceReport { get; private set; }
	public Expression PublicServiceReport { get; private set; }

    public Expression AgricultureAdvice { get; private set; }
    public Expression CommerceAdvice { get; private set; }
    public Expression PublicServiceAdvice { get; private set; }


    private string _agricultureMarkup;
    private string _commerceMarkup;
    private string _publicServiceMarkup;


	public DialogState() : base()
	{

	}

	public DialogState(DialogState state)
	{
		AgricultureReport = state.AgricultureReport;
		CommerceReport = state.CommerceReport;
		PublicServiceReport = state.PublicServiceReport;
	    AgricultureAdvice = state.AgricultureAdvice;
	    CommerceAdvice = state.CommerceAdvice;
	    PublicServiceAdvice = state.PublicServiceAdvice;

	}

    public override void AdvanceState()
    {
        string reportMarkup = GenerateReportMarkup();

        var sb = new StringBuilder();
        sb.Append("Assignment_Agriculture;");
        sb.Append(reportMarkup + ";");
        sb.Append(GameState.CharacterState.AgricultureAdvisor.GenerateAdvisorMarkup());
        _agricultureMarkup = sb.ToString();
        GameState.CharacterState.AgricultureAdvisor.Report = AgricultureReport = 
            GameState.CharacterState.AgricultureAdvisor.GenerateReport(_agricultureMarkup);

        sb = new StringBuilder();
        sb.Append("Assignment_Commerce;");
        sb.Append(reportMarkup + ";");
        sb.Append(GameState.CharacterState.CommerceAdvisor.GenerateAdvisorMarkup());
        _commerceMarkup = sb.ToString();
        GameState.CharacterState.CommerceAdvisor.Report = CommerceReport =
          GameState.CharacterState.CommerceAdvisor.GenerateReport(_commerceMarkup);


        sb = new StringBuilder();
        sb.Append("Assignment_PublicService;");
        sb.Append(reportMarkup + ";");
        sb.Append(GameState.CharacterState.CommerceAdvisor.GenerateAdvisorMarkup());
        _publicServiceMarkup = sb.ToString();
        GameState.CharacterState.PublicServiceAdvisor.Report = PublicServiceReport =
            GameState.CharacterState.PublicServiceAdvisor.GenerateReport(_publicServiceMarkup);

	}

    public string GenerateReportMarkup()
    {
        string markup = "";

        //Generate Markup based on Agriculture State
		markup += "HarvestYield_";
		if (GameState.GlobalState.NetHarvest > BalanceManager.instance.Thresholds.HarvestYield_upper) {
			markup += "High";
		} else if (GameState.GlobalState.NetHarvest < BalanceManager.instance.Thresholds.HarvestYield_lower) {
			markup += "Low";
		} else 
		{
			markup += "Medium";
		}


               
        if (GameState.AgricultureState.WorkableLand > 0) // Prevent Divive By Zero
        {
			markup += ";PlantedRatio_";
			if ((float)GameState.AgricultureState.SeededLand / GameState.AgricultureState.WorkableLand >
			    BalanceManager.instance.Thresholds.PlantedRatio_upper) {
				// Planted ratio is above upper threshold
				markup += "High";
			} else if ((float)GameState.AgricultureState.SeededLand / GameState.AgricultureState.WorkableLand <
			           BalanceManager.instance.Thresholds.PlantedRatio_lower) {
				// Planted ratio is below lower threshold
				markup += "Low";
			} else // Planted ratio is between upper and lower threshold
			{
				markup += "Medium";
			}     
        }


		markup += ";ProductionModifier_";
		if (GameState.AgricultureState.ProductionModifier > BalanceManager.instance.Thresholds.ProdModifier_upper) {
			markup += "High";
		} else if (GameState.AgricultureState.ProductionModifier < BalanceManager.instance.Thresholds.ProdModifier_lower) {
			markup += "Low";
		} else 
		{
			markup += "Medium";
		}

        // Generate markup based on Commerce State
		if (GameState.CommerceState.Action == CommerceAction.Buy) {
			markup += ";LandBought_";
			if (GameState.CommerceState.Budget > BalanceManager.instance.Thresholds.LandBought_upper) {
				markup += "High";
			} else if (GameState.CommerceState.Budget < BalanceManager.instance.Thresholds.LandBought_lower) {
				markup += "Low";
			} else 
			{
				markup += "Medium";
			}

		} else if (GameState.CommerceState.Action == CommerceAction.Sell) {
			markup += ";LandSold_";
			if (GameState.CommerceState.Budget > BalanceManager.instance.Thresholds.LandSold_upper) {
				markup += "High";
			} else if (GameState.CommerceState.Budget < BalanceManager.instance.Thresholds.LandSold_lower) {
				markup += "Low";
			} else 
			{
				markup += "Medium";
			}
		} 

        // Generate markup based on Public Service State
		markup+=";Fed_";
		if (GameState.PublicServiceState.Starvations == 0) {
			if (GameState.PublicServiceState.OverfeedPercent > BalanceManager.instance.Thresholds.Overfeed) {
				markup += "Over";
			} else {
				markup += "Exact";
			}
		}
		else if (GameState.PublicServiceState.Starvations > BalanceManager.instance.Thresholds.Starvations_upper)
        {
            markup += "StarvedMedium";
        }
        else 
        {
            markup += "StarvedLow";
        }



        // Generate markup based on rats state
		markup+=";Rats_";
        if (GameState.RatsState.RatsPercent == 0)
	    {
	        markup += "None";
	    }
		else if (GameState.RatsState.RatsPercent > BalanceManager.instance.Thresholds.Rats)
	    {
	        markup += "High";
	    }
	    else
	    {
	        markup += "Low";
	    }


           

        // Generate markup based on crisis state
		markup+=";Crisis_";
	    if (GameState.CrisesState.ActiveCrisis != null)
	    {
	        markup += GameState.CrisesState.ActiveCrisis.Name;
	    }
	    else
	    {
	        markup += "None";
	    }


		// Generate markup based on King Land, Grain
		markup+=";KingGrain_";
		if (GameState.GlobalState.GrainStore > BalanceManager.instance.Thresholds.KingGrain_upper) {
			markup += "High";
		} else if (GameState.GlobalState.GrainStore < BalanceManager.instance.Thresholds.KingGrain_lower) {
			markup += "Low";
		} else {
			markup += "Medium";
		}

		markup += ";KingLand_";
		if (GameState.GlobalState.Acreage > BalanceManager.instance.Thresholds.KingLand_upper) {
			markup += "High";
		} else if (GameState.GlobalState.Acreage < BalanceManager.instance.Thresholds.KingLand_lower) {
			markup += "Low";
		} else {
			markup += "Medium";
		}


		markup += ";KingApproval_";
		if (GameState.GlobalState.KingApproval > BalanceManager.instance.Thresholds.KingApproval_upper) {
			markup += "High";
		} else if (GameState.GlobalState.KingApproval < BalanceManager.instance.Thresholds.KingApproval_lower) {
			markup += "Low";
		} else {
			markup += "Medium";
		}
        return markup;
    }

    public string GenerateAdviceMarkup()
    {

        var sb = new StringBuilder();

        // Generate advice markup based on Agriculture State
        sb.Append("Workers_");

        if (GameState.GlobalState.Population / GameState.GlobalState.Acreage >
            BalanceManager.instance.Thresholds.Workers_upper)
        {
            sb.Append("Over");
        }
        else if (GameState.GlobalState.Population / GameState.GlobalState.Acreage <
                 BalanceManager.instance.Thresholds.Workers_lower)
        {
            sb.Append("Under");
        }
        else
        {
            sb.Append("Exact");
        }

        // Generate Advice markup based on public service state
        sb.Append(";Population_");
        if (GameState.GlobalState.Population > BalanceManager.instance.Thresholds.Population_Upper)
        {
            sb.Append("High");
        }
        else if (GameState.GlobalState.Population < BalanceManager.instance.Thresholds.Population_Lower)
        {
            sb.Append("Low");
        }
        else
        {
            sb.Append("Medium");
        }

        // Generate advice markup based on Agriculture State
        sb.Append(";LandPrice_");
        if (GameState.CommerceState.PriceOfLand > BalanceManager.instance.Thresholds.LandPrice_upper)
        {
            sb.Append("H");
        }
        else if (GameState.CommerceState.PriceOfLand < BalanceManager.instance.Thresholds.LandPrice_lower)
        {
            sb.Append("H");
        }
        else
        {
            sb.Append("M");
        }
        return sb.ToString();
    }

	/**
	 * Generate markup specific to each advisor for report
	 * 
	 **/
	//private string GenerateAdditionalMarkup(string assignment) {
	//	string additionalMarkup = "";
	//	switch (assignment) {
	//		case "Agriculture":
	//			// Generate AdvisorLand, AdvisorGrain markup for agriculture advisor
	//			additionalMarkup += ";AdvisorGrain_";
	//			if (GameState.CharacterState.AgricultureAdvisor.Grain > BalanceManager.instance.Thresholds.AdvisorGrain_upper) {
	//				additionalMarkup += "High";
	//			} else if (GameState.CharacterState.AgricultureAdvisor.Grain < BalanceManager.instance.Thresholds.AdvisorGrain_lower) {
	//				additionalMarkup += "Low";
	//			} else {
	//				additionalMarkup += "Medium";
	//			}

	//			additionalMarkup += ";AdvisorLand_";
	//			if (GameState.CharacterState.AgricultureAdvisor.Land > BalanceManager.instance.Thresholds.AdvisorLand_upper) {
	//				additionalMarkup += "High";
	//			} else if (GameState.CharacterState.AgricultureAdvisor.Land < BalanceManager.instance.Thresholds.AdvisorLand_lower) {
	//				additionalMarkup += "Low";
	//			} else {
	//				additionalMarkup += "Medium";
	//			}

	//			additionalMarkup += ";AdvisorApproval_";
	//			if (GameState.CharacterState.AgricultureAdvisor.Approval > BalanceManager.instance.Thresholds.AdvisorApproval_upper) {
	//				additionalMarkup += "High";
	//			} else if (GameState.CharacterState.AgricultureAdvisor.Approval < BalanceManager.instance.Thresholds.AdvisorApproval_lower) {
	//				additionalMarkup += "Low";
	//			} else {
	//				additionalMarkup += "Medium";
	//			}
	//			break;
	//		case "Commerce":
	//			// Generate AdvisorLand, AdvisorGrain markup for commerce advisor
	//			additionalMarkup += ";AdvisorGrain_";
	//			if (GameState.CharacterState.CommerceAdvisor.Grain > BalanceManager.instance.Thresholds.AdvisorGrain_upper) {
	//				additionalMarkup += "High";
	//			} else if (GameState.CharacterState.CommerceAdvisor.Grain < BalanceManager.instance.Thresholds.AdvisorGrain_lower) {
	//				additionalMarkup += "Low";
	//			} else {
	//				additionalMarkup += "Medium";
	//			}

	//			additionalMarkup += ";AdvisorLand_";
	//			if (GameState.CharacterState.CommerceAdvisor.Land > BalanceManager.instance.Thresholds.AdvisorLand_upper) {
	//				additionalMarkup += "High";
	//			} else if (GameState.CharacterState.CommerceAdvisor.Land < BalanceManager.instance.Thresholds.AdvisorLand_lower) {
	//				additionalMarkup += "Low";
	//			} else {
	//				additionalMarkup += "Medium";
	//			}
	//			additionalMarkup += ";AdvisorApproval_";
	//			if (GameState.CharacterState.CommerceAdvisor.Approval > BalanceManager.instance.Thresholds.AdvisorApproval_upper) {
	//				additionalMarkup += "High";
	//			} else if (GameState.CharacterState.CommerceAdvisor.Approval < BalanceManager.instance.Thresholds.AdvisorApproval_lower) {
	//				additionalMarkup += "Low";
	//			} else {
	//				additionalMarkup += "Medium";
	//			}


	//			break;
	//		case "PublicService":
	//			// Generate AdvisorLand, AdvisorGrain markup for Public Service advisor
	//			additionalMarkup += ";AdvisorGrain_";
	//			if (GameState.CharacterState.PublicServiceAdvisor.Grain > BalanceManager.instance.Thresholds.AdvisorGrain_upper) {
	//				additionalMarkup += "High";
	//			} else if (GameState.CharacterState.PublicServiceAdvisor.Grain < BalanceManager.instance.Thresholds.AdvisorGrain_lower) {
	//				additionalMarkup += "Low";
	//			} else {
	//				additionalMarkup += "Medium";
	//			}

	//			additionalMarkup += ";AdvisorLand_";
	//			if (GameState.CharacterState.PublicServiceAdvisor.Land > BalanceManager.instance.Thresholds.AdvisorLand_upper) {
	//				additionalMarkup += "High";
	//			} else if (GameState.CharacterState.PublicServiceAdvisor.Land < BalanceManager.instance.Thresholds.AdvisorLand_lower) {
	//				additionalMarkup += "Low";
	//			} else {
	//				additionalMarkup += "Medium";
	//			}
	//			additionalMarkup += ";AdvisorApproval_";
	//			if (GameState.CharacterState.PublicServiceAdvisor.Approval > BalanceManager.instance.Thresholds.AdvisorApproval_upper) {
	//				additionalMarkup += "High";
	//			} else if (GameState.CharacterState.PublicServiceAdvisor.Approval < BalanceManager.instance.Thresholds.AdvisorApproval_lower) {
	//				additionalMarkup += "Low";
	//			} else {
	//				additionalMarkup += "Medium";
	//			}
	//			break;

	//		default:
	//			break;
	//	}
	//	return additionalMarkup;
	//}
	
	public override string ToString()
	{
		return String.Format(
			"_Dialog State_\n\n" +
			"AgricultureReport:\n{0}\n" +
            "AgricultureMarkup:\n{1}\n" +
            "CommerceReport:\n{2}\n" +
			"CommerceMarkup:\n{3}\n" +
            "PublicServiceReport:\n{4}\n" +
            "PublicServiceMarkup:\n{5}\n"
            ,
			AgricultureReport,
            _agricultureMarkup,
            CommerceReport,
            _commerceMarkup,
            PublicServiceReport,
            _publicServiceMarkup
		);
	}
}
