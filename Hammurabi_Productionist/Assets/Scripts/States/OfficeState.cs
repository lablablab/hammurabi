﻿using UnityEngine;
using System;
using System.Collections;

public abstract class OfficeState : State
{
	public const string kNegativeFlag = "_Negative";
	public const string kPositiveFlag = "_Positive";
	public const string kNeutralFlag = "_Neutral";

	public const string kTemperingFlagset = "Tempering";
	public const string kCompetenceFlagset = "Competence";
	
	public enum BudgetModifier
	{
		Neutral = 0,
		Negative = 1,
		Positive = 2
	}

	public BudgetModifier TemperingModifier;
	public BudgetModifier CompetenceModifier;
	
	public Character Advisor;

	public override void AdvanceState()
	{
		TemperingModifier = CheckAdvisorFlags(kTemperingFlagset);
		CompetenceModifier = CheckAdvisorFlags(kCompetenceFlagset);
	}

	private BudgetModifier CheckAdvisorFlags(string flagSet)
	{
		int negativeScore = 0;
		int positiveScore = 0;
		int neutralScore = 0;

		string[] flags = Advisor.Decision.Flags;
		for (int i = 0; i < flags.Length; i++)
		{
			if(flags[i] == flagSet + kNegativeFlag)
			{
				negativeScore++;
			}
			else if(flags[i] == flagSet + kPositiveFlag)
			{
				positiveScore++;
			}
			else if(flags[i] == flagSet + kNeutralFlag)
			{
				neutralScore++;
			}
		}

		if (negativeScore > 0 || positiveScore > 0)
		{
			int totalScore = negativeScore + positiveScore + neutralScore;
			int rnd = UnityEngine.Random.Range(0, totalScore);

			if (rnd < negativeScore)
			{
				return BudgetModifier.Negative;
			}
			else if (rnd < totalScore - neutralScore)
			{
				return BudgetModifier.Positive;
			}
		}

		return BudgetModifier.Neutral;
	}
}

