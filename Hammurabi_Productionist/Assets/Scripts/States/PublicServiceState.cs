﻿using UnityEngine;
using System;
using System.Collections;

public class PublicServiceState : OfficeState
{
	private PublicServiceBalance _balanceUnit = BalanceManager.instance.PublicService;

	public int Budget { get; private set; }
	public int Tempering { get; private set; }
	public int Starvations { get; private set; }
	public bool OverfeedCheck { get; private set; }
	public float OverfeedPercent { get; private set; }
	public int Immigration { get; private set; }

	public PublicServiceState() : base()
	{
		
	}

	public PublicServiceState(PublicServiceState state)
	{
		Budget = state.Budget;
		Tempering = state.Tempering;
		Starvations = state.Starvations;
		OverfeedCheck = state.OverfeedCheck;
		OverfeedPercent = state.OverfeedPercent;
		Immigration = state.Immigration;
	}

	public override void AdvanceState()
	{
		// Base variables fetching

		Advisor = GameState.CharacterState.PublicServiceAdvisor;

		base.AdvanceState();

		int ActualGrainToFeedPopulationUnit = _balanceUnit.GrainToFeedPopulationUnit;

		Budget = int.Parse(TurnPanel.instance.PublicServiceBudget.text);
		GameState.GlobalState.GrainStore -= Budget;

		// Base variables adjustments

		Tempering = 0;

		if (TemperingModifier == BudgetModifier.Negative)
		{
			Tempering = -(int)(Budget * _balanceUnit.StealPercentage);
		}
		else if (TemperingModifier == BudgetModifier.Positive)
		{
			Tempering = (int)(GameState.CharacterState.PublicServiceAdvisor.Grain * _balanceUnit.GivePercentage);
		}

		Budget += Tempering;
		Advisor.Grain -= Tempering;

		if (CompetenceModifier == BudgetModifier.Negative)
		{
			ActualGrainToFeedPopulationUnit += _balanceUnit.CompetenceModifier;
		}
		else if (CompetenceModifier == BudgetModifier.Positive)
		{
			ActualGrainToFeedPopulationUnit -= _balanceUnit.CompetenceModifier;
		}

		// Additional variables computations

		Starvations = Math.Max(0, GameState.GlobalState.Population - (Budget / ActualGrainToFeedPopulationUnit));

		OverfeedCheck = (Budget / ActualGrainToFeedPopulationUnit) >= GameState.GlobalState.Population * _balanceUnit.OverfeedThreshold;

		OverfeedPercent = OverfeedCheck ? ((float)Budget / (float)(Math.Max(GameState.GlobalState.Population, 1) * ActualGrainToFeedPopulationUnit)) - 1f : 0f;

		Immigration = Math.Max(UnityEngine.Random.Range(_balanceUnit.ImmigrationRndLow, _balanceUnit.ImmigrationRndHigh), (GameState.GlobalState.Acreage / Math.Max(GameState.GlobalState.Population, 1)) - 10);
		Immigration += OverfeedCheck ? (int)(GameState.PublicServiceState.OverfeedPercent * GameState.GlobalState.Population / _balanceUnit.ImmigrationDiviser) : 0;
		
		// Base variables adjustments

		int approvalModifier = 0;

		if (OverfeedCheck)
		{
			GameState.GlobalState.Happiness += _balanceUnit.HappinessIncrement;
			approvalModifier = _balanceUnit.BaseApprovalIncrement;

			if (GameState.LastTurn != null &&
				GameState.LastTurn.CharacterState.PublicServiceAdvisor == GameState.CharacterState.PublicServiceAdvisor &&
				GameState.CharacterState.PublicServiceAdvisor.Approval > GameState.LastTurn.CharacterState.PublicServiceAdvisor.Approval)
			{
				approvalModifier = _balanceUnit.AdvancedApprovalIncrement;
			}
		}
		
		if (Starvations > 0)
		{
			GameState.GlobalState.Happiness -= _balanceUnit.HappinessDecrement;
			approvalModifier = -(_balanceUnit.ApprovalDecrement);
		}
		
		GameState.GlobalState.Population -= Starvations;
		GameState.GlobalState.Population += Immigration;
		GameState.CharacterState.PublicServiceAdvisor.Approval += approvalModifier;
	}

	public override string ToString()
	{
		return String.Format(
			"_Public Service State_ Budget: {0}, Tempering: {1}{2}, Starvations: {3}, OverfeedCheck: {4}, OverfeedPercent: {5}, Immigration: {6}{7}{8}",
			Budget,
			TemperingModifier == BudgetModifier.Positive ? "+" : "",
			Tempering,
			Starvations,
			OverfeedCheck,
			OverfeedPercent,
			Immigration,
			CompetenceModifier == BudgetModifier.Positive ? " (Competence Bonus)" : "",
			CompetenceModifier == BudgetModifier.Negative ? " (Incompetence `Malus)" : ""
		);
	}

}
