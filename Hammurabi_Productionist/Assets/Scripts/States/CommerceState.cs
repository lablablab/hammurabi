﻿using UnityEngine;
using System;
using System.Collections;

public enum CommerceAction
{
	None = 0,
	Buy = 1,
	Sell = 2
}

public class CommerceState : OfficeState {

	private CommerceBalance _balanceUnit = BalanceManager.instance.Commerce;

	public int PriceOfLand { get; private set; }
	public CommerceAction Action { get; private set; }
	public int Budget { get; private set; }
	public int Tempering { get; private set; }
	public int Earnings { get; private set; }

	public CommerceState() : base()
	{
		
	}

	public CommerceState(CommerceState state)
	{
		Action = state.Action;
		PriceOfLand = state.PriceOfLand;
		Budget = state.Budget;
		Tempering = state.Tempering;
		Earnings = state.Earnings;
	}

	public override void AdvanceState()
	{
		// Base variables fetching

		Advisor = GameState.CharacterState.CommerceAdvisor;

		base.AdvanceState();

		PriceOfLand = (GameState.GlobalState.TotalLand / GameState.GlobalState.OtherLand) + UnityEngine.Random.Range(_balanceUnit.PriceOfLandRndLow, _balanceUnit.PriceOfLandRndHigh) + _balanceUnit.PriceOfLandModifier;

		Action = (CommerceAction)(TurnPanel.instance.CommerceAction.value);

		Budget = int.Parse(TurnPanel.instance.CommerceBudget.text);
		
		if (Action == CommerceAction.Buy)
		{
			GameState.GlobalState.GrainStore -= Budget;

			// Base variables adjustments

			Tempering = 0;

			if (TemperingModifier == BudgetModifier.Negative)
			{
				Tempering = -(int)(Budget * _balanceUnit.GrainStealPercentage);
			}
			else if (TemperingModifier == BudgetModifier.Positive)
			{
				Tempering = (int)(GameState.CharacterState.CommerceAdvisor.Grain * _balanceUnit.GrainGivePercentage);
			}

			Budget += Tempering;
			Advisor.Grain -= Tempering;

			if (CompetenceModifier == BudgetModifier.Negative)
			{
				PriceOfLand -= _balanceUnit.CompetenceModifier;
			}
			else if (CompetenceModifier == BudgetModifier.Positive)
			{
				PriceOfLand += _balanceUnit.CompetenceModifier;
			}

			// Additional variables computations

			Earnings = Budget / PriceOfLand;

			// Global variables adjustments

			GameState.GlobalState.Acreage += Earnings;
		}
		else if (Action == CommerceAction.Sell)
		{
			GameState.GlobalState.Acreage -= Budget;

			// Base variables adjustments

			if (TemperingModifier == BudgetModifier.Negative)
			{
				Tempering = -(int)(Budget * _balanceUnit.LandStealPercentage);
			}
			else if (TemperingModifier == BudgetModifier.Positive)
			{
				Tempering = (int)(GameState.CharacterState.CommerceAdvisor.Grain * _balanceUnit.LandGivePercentage);
			}

			Budget += Tempering;
			Advisor.Land -= Tempering;

			if (CompetenceModifier == BudgetModifier.Negative)
			{
				PriceOfLand += _balanceUnit.CompetenceModifier;
			}
			else if (CompetenceModifier == BudgetModifier.Positive)
			{
				PriceOfLand -= _balanceUnit.CompetenceModifier;
			}

			// Additional variables computations

			Earnings = (Budget * PriceOfLand) + (Budget % PriceOfLand);

			// Global variables adjustments

			GameState.GlobalState.GrainStore += Earnings;
		}
	}

	public override string ToString()
	{
		string budgetType;
		string earningsType;
		
		if (Action == CommerceAction.Buy)
		{
			budgetType = "grain";
			earningsType = "land";
		}
		else if (Action == CommerceAction.Sell)
		{
			budgetType = "land";
			earningsType = "grain";
		}
		else
		{
			budgetType = "...";
			earningsType = "...";
		}

		return String.Format(
			"_Commerce State_ PriceOfLand: {0}, Action: {1}, Budget: {2} ({3}), Tempering: {4}{5}, Earnings: {6} ({7}){8}{9}",
			PriceOfLand,
			Action.ToString(),
			Budget,
			budgetType,
			TemperingModifier == BudgetModifier.Positive ? "+" : "",
			Tempering,
			Earnings,
			earningsType,
			CompetenceModifier == BudgetModifier.Positive ? " (Competence Bonus)" : "",
			CompetenceModifier == BudgetModifier.Negative ? " (Incompetence `Malus)" : ""
		);
	}

}
