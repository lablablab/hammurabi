﻿using UnityEngine;
using System.Collections;

public abstract class State
{
	public GameStateManager GameState;
	public abstract void AdvanceState();

	public State()
	{
		GameState = GameStateManager.instance;
	}
}
