﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Crisis
{
	public string Name { get; private set; }
	public int Threshold { get; set; }
	public Action Trigger { get; private set; }

	public Crisis(string name, int threshold, Action trigger)
	{
		Name = name;
		Threshold = threshold;
		Trigger = trigger;
	}
}

public class CrisesState : State {

	private CrisesBalance _balanceUnit = BalanceManager.instance.Crises;

	public const string kEarthquake = "Earthquake";
	public const string kLocusts = "Locusts";
	public const string kPlague = "Plague";
	//public const string kWildfire = "Wildfire";

	public Crisis ActiveCrisis { get; private set; }

	private List<Crisis> _crises;
	private int _crisisTimerIncrement;
	private int _crisisTimer;
	
	public CrisesState() : base()
	{
		_crises = new List<Crisis>();

		_crisisTimerIncrement = _balanceUnit.BaseTimerIncrement;
		_crisisTimer = 0;

		Action EarthquakeConsequences = () => {
			GameState.GlobalState.Population = (int)(GameState.GlobalState.Population * _balanceUnit.EarthquakeLandModifier);
			GameState.GlobalState.Acreage = (int)(GameState.GlobalState.Acreage * _balanceUnit.EarthquakeLandModifier);
			GameState.GlobalState.OtherLand = (int)(GameState.GlobalState.OtherLand * _balanceUnit.EarthquakeLandModifier);
			foreach (Character Character in GameState.CharacterState.Characters)
			{
				Character.Land = (int)(Character.Land * _balanceUnit.EarthquakeLandModifier);
			}
		};

		Action LocustsConsequences = () => {
			GameState.GlobalState.GrainStore = (int)(GameState.GlobalState.GrainStore * _balanceUnit.LocustGrainModifier);
			foreach(Character Character in GameState.CharacterState.Characters)
			{
				Character.Grain = (int)(Character.Grain * _balanceUnit.LocustGrainModifier);
			}
		};

		Action PlagueConsequences = () => {
			GameState.GlobalState.Population = (int)(GameState.GlobalState.Population * _balanceUnit.PlaguePopulationModifier);
		};

		//Action WildfireConsequences = () => {
		//	// TODO: implement "renders 25% of the king's land unusable until the end of the turn"
		//};

		_crises.Add(new Crisis(kEarthquake, _balanceUnit.EarthquakeBaseThreshold, EarthquakeConsequences));
		_crises.Add(new Crisis(kLocusts, _balanceUnit.LocustBaseThreshold, LocustsConsequences));
		_crises.Add(new Crisis(kPlague, _balanceUnit.PlagueBaseThreshold, PlagueConsequences));
		//_crises.Add(new Crisis(kWildfire, 4, WildfireConsequences));

		ActiveCrisis = null;
	}

	public CrisesState(CrisesState state)
	{
		ActiveCrisis = state.ActiveCrisis;
	}

	public override void AdvanceState()
	{
		_crisisTimer += _crisisTimerIncrement;

		Crisis crisisToTrigger = null;

		foreach(Crisis crisis in _crises)
		{
			if(CheckCrisisTimer(crisis) && (crisisToTrigger == null || crisis.Threshold < crisisToTrigger.Threshold))
			{
				crisisToTrigger = crisis;
			}
		}

		if(crisisToTrigger == null)
		{
			ActiveCrisis = null;
			return;
		}

		ActiveCrisis = TriggerCrisis(crisisToTrigger);

		GameState.GlobalState.Happiness -= ActiveCrisis != null ? _balanceUnit.HappinessDecrement : 0;
	}
	
	public bool CheckCrisisTimer(Crisis crisis)
	{
		if (_crisisTimer >= crisis.Threshold)
		{
			float rng = (_crisisTimer - crisis.Threshold) * _balanceUnit.RngMultiplier;
			float rnd = UnityEngine.Random.Range(_balanceUnit.TimerRndLow, _balanceUnit.TimerRndHigh);

			return rng >= rnd;
		}
		return false;
	}
	
	public Crisis TriggerCrisis(Crisis crisis)
	{
		crisis.Trigger();
		crisis.Threshold *= _balanceUnit.ThresholdMultiplier;

		_crisisTimer = 0;
		_crisisTimerIncrement *= _balanceUnit.TimerIncrementMutliplier;

		return crisis;
	}

	public override string ToString()
	{
		return String.Format(
			"_Crises State_ Crisis: {0}", 
			ActiveCrisis != null ? ActiveCrisis.Name : "none"
		);
	}
}