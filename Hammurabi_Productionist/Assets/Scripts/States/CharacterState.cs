﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using System.Reflection;
using System.Text;

public class Character
{
	public string Name;
	public Productionist IntroductionGrammar;
	public Productionist AdviceGrammar;
	public Productionist DecisionGrammar;
	public Productionist ReportGrammar;
	public Action Trait;
	public int Approval;
	public int Grain;
	public int Land;
	public string[] Traits;

	public Expression Introduction { get; set; }
	public Expression Advice { get; set; }
	public Expression Decision { get; set; }
	public Expression Report { get; set; }
	
	public Character(string name, Productionist introductionGrammar, Productionist adviceGrammar, Productionist decisionGrammar, Productionist reportGrammar, int approval, int grain, int land)
	{
		Name = name;
		IntroductionGrammar = introductionGrammar;
		AdviceGrammar = adviceGrammar;
		DecisionGrammar = decisionGrammar;
		ReportGrammar = reportGrammar;
		Approval = approval;
		Grain = grain;
	    Land = land;

		List<string> tempTraits = new List<string>();
		string[] traitTags;
		ReportGrammar.Root.markups.TryGetValue("Traits", out traitTags);
		if (traitTags != null)
		{
			for (int j = 0; j < traitTags.Length; j++)
			{
				tempTraits.Add(traitTags[j]);
			}

			Traits = tempTraits.ToArray();
		}

		Introduction = IntroductionGrammar.GenerateExpression();
	}
	
	public Expression GenerateDecision(string assignment)
	{
        var sb = new StringBuilder();

        sb.Append("Assignment_" + assignment);
        sb.Append(GenerateAdvisorMarkup());

        return DecisionGrammar.GenerateExpression(sb.ToString());
	}

    public Expression GenerateReport(string markup)
    {
        return ReportGrammar.GenerateExpression(markup);
    }

    public Expression GenerateAdvice(string markup)
    {
        return AdviceGrammar.GenerateExpression(markup);
    }

    public string GenerateAdvisorMarkup()
    {
        var sb = new StringBuilder();
        sb.Append("AdvisorGrain_");
        if (Grain > BalanceManager.instance.Thresholds.AdvisorGrain_upper)
        { 
            sb.Append("High");
        }
        else if (Grain < BalanceManager.instance.Thresholds.AdvisorGrain_lower)
        {
            sb.Append("Low");
        }
        else
        {
            sb.Append("Medium");
        }


        sb.Append(";AdvisorLand_");
        if (Land > BalanceManager.instance.Thresholds.AdvisorLand_upper)
        {
            sb.Append("High");
        }
        else if (Land < BalanceManager.instance.Thresholds.AdvisorLand_lower)
        {
            sb.Append("Low");
        }
        else
        {
            sb.Append("Medium");
        }

        sb.Append(";AdvisorApproval_");
        if (Approval > BalanceManager.instance.Thresholds.AdvisorApproval_upper)
        {
            sb.Append("High");
        }
        else if (Approval < BalanceManager.instance.Thresholds.AdvisorApproval_lower)
        {
            sb.Append("Low");
        }
        else
        {
            sb.Append("Medium");
        }

        return sb.ToString();
    }
}

public class CharacterState : State {

	private CharacterBalance _balanceUnit = BalanceManager.instance.Characters;

	public Character AgricultureAdvisor { get; set; }
	public Character CommerceAdvisor { get; set; }
	public Character PublicServiceAdvisor { get; set; }

	public List<Character> Characters { get; private set; }
	
	public CharacterState() : base()
	{
		Characters = new List<Character>();

		Characters.Add(new Character(
			_balanceUnit.Advisor1Name,
			DialogManager.instance.Productionists[_balanceUnit.Advisor1IntroductionGrammar],
			DialogManager.instance.Productionists[_balanceUnit.Advisor1AdviceGrammar],
			DialogManager.instance.Productionists[_balanceUnit.Advisor1DecisionGrammar],
			DialogManager.instance.Productionists[_balanceUnit.Advisor1ReportGrammar],
			_balanceUnit.Advisor1Approval,
			_balanceUnit.Advisor1Grain,
			_balanceUnit.Advisor1Land
		));
		
		Characters.Add(new Character(
			_balanceUnit.Advisor2Name,
			DialogManager.instance.Productionists[_balanceUnit.Advisor2IntroductionGrammar],
			DialogManager.instance.Productionists[_balanceUnit.Advisor2AdviceGrammar],
			DialogManager.instance.Productionists[_balanceUnit.Advisor2DecisionGrammar],
			DialogManager.instance.Productionists[_balanceUnit.Advisor2ReportGrammar], 
			_balanceUnit.Advisor2Approval, 
			_balanceUnit.Advisor2Grain,
			_balanceUnit.Advisor2Land
		));

		Characters.Add(new Character(
			_balanceUnit.Advisor3Name,
			DialogManager.instance.Productionists[_balanceUnit.Advisor3IntroductionGrammar],
			DialogManager.instance.Productionists[_balanceUnit.Advisor3AdviceGrammar],
			DialogManager.instance.Productionists[_balanceUnit.Advisor3DecisionGrammar],
			DialogManager.instance.Productionists[_balanceUnit.Advisor3ReportGrammar],
			_balanceUnit.Advisor3Approval, 
			_balanceUnit.Advisor3Grain, 
			_balanceUnit.Advisor3Land
		));
	}

	public CharacterState(CharacterState state)
	{
		AgricultureAdvisor = state.AgricultureAdvisor;
		CommerceAdvisor = state.CommerceAdvisor;
		PublicServiceAdvisor = state.PublicServiceAdvisor;
		Characters = state.Characters;
	}

	public override void AdvanceState()
	{
		for(int i = 0; i < Characters.Count; i++)
		{
			if(Characters[i].Traits == null)
			{
				continue;
			}

			for (int j = 0; j < Characters[i].Traits.Length; j++)
			{
				MethodInfo trait = TraitsManager.instance.GetType().GetMethod(Characters[i].Traits[j]);
				if(trait == null)
				{
					continue;
				}

				object[] parameters = new object[1];
				parameters[0] = Characters[i];

				trait.Invoke(TraitsManager.instance, parameters);
			}
		}

		AgricultureAdvisor.Decision = AgricultureAdvisor.GenerateDecision("Agriculture");
		CommerceAdvisor.Decision = CommerceAdvisor.GenerateDecision("Commerce");
		PublicServiceAdvisor.Decision = PublicServiceAdvisor.GenerateDecision("PublicService");
	}

	public Character GetCharacterByOffice(string office)
	{
		switch (office)
		{
			case "Agriculture":
				return AgricultureAdvisor;
			case "Commerce":
				return CommerceAdvisor;
			case "PublicService":
				return PublicServiceAdvisor;
			default:
				return null;
		}
	}
	
	public override string ToString()
	{
		return String.Format(
			"_Character State_ " + 
			"Agriculture Advisor: {0}(Grain: {1}, Land: {2}, Approval: {3}), " +
			"Commerce Advisor: {4}(Grain: {5}, Land: {6}, Approval: {7}), " +
			"Public Service Advisor: {8}(Grain: {9}, Land: {10}, Approval: {11})",
			AgricultureAdvisor.Name,
			AgricultureAdvisor.Grain,
			AgricultureAdvisor.Land,
			AgricultureAdvisor.Approval,
			CommerceAdvisor.Name,
			CommerceAdvisor.Grain,
			CommerceAdvisor.Land,
			CommerceAdvisor.Approval,
			PublicServiceAdvisor.Name,
			PublicServiceAdvisor.Grain,
			PublicServiceAdvisor.Land,
			PublicServiceAdvisor.Approval
		);
	}
}
