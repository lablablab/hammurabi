﻿using UnityEngine;
using System;
using System.Collections;

public class RatsState : State
{
	private RatsBalance _balanceUnit = BalanceManager.instance.Rats;

	public float RatsPercent { get; private set; }
	public int GlobalGrainLoss { get; private set; }

	public RatsState() : base()
	{
	}

	public RatsState(RatsState state)
	{
		RatsPercent = state.RatsPercent;
	}

	public override void AdvanceState()
	{
		if(GameState.CrisesState.ActiveCrisis != null 
		&& GameState.CrisesState.ActiveCrisis.Name == CrisesState.kLocusts)
		{
			RatsPercent = 0f;
			return;
		}

		float rnd = UnityEngine.Random.Range(_balanceUnit.RndLow, _balanceUnit.RndHigh);

		if (rnd > _balanceUnit.Threshold1)
		{
			RatsPercent = _balanceUnit.Modifier1;
		}
		else if (rnd > _balanceUnit.Threshold2)
		{
			RatsPercent = _balanceUnit.Modifier2;
		}
		else if (rnd > _balanceUnit.Threshold3)
		{
			RatsPercent = _balanceUnit.Modifier3;
		}
		else if (rnd > _balanceUnit.Threshold4)
		{
			RatsPercent = _balanceUnit.Modifier4;
		}
		else
		{
			RatsPercent = 0f;
		}

		GlobalGrainLoss = (int)(GameState.GlobalState.GrainStore * RatsPercent);
		GameState.GlobalState.GrainStore -= GlobalGrainLoss;

		foreach (Character character in GameState.CharacterState.Characters)
		{
			int characterGrainLoss = (int)(character.Grain * RatsPercent);
			character.Grain -= characterGrainLoss;
		}
	}

	public override string ToString()
	{
		return String.Format(
			"_Rats State_ RatsPercent: {0}, GlobalGrainLoss: {1}",
			RatsPercent,
			GlobalGrainLoss
		);
	}
}
