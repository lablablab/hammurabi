﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraitsManager : MonoBehaviour {

	public static TraitsManager instance = null;

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
		}
	}

	public void ExampleTrait1(Character character)
	{
		Debug.Log(character.Name + "(1)");
	}

	public void ExampleTrait2(Character character)
	{
		Debug.Log(character.Name + "(2)");
	}

	public void ExampleTrait3(Character character)
	{
		Debug.Log(character.Name + "(3)");
	}
}
