﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour {

	public ExpressionPanel ExpressionPanel;

	public static UIManager instance = null;

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
		}
	}

	void Start()
	{
		
	}
}
