﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using System.Linq;

public class DialogManager : MonoBehaviour {
	
	public static DialogManager instance = null;
	
    public const string kImpossibleExpression = "<ImpossibleExpression>";

	public List<string> MustHave;
	public List<string> MustNotNotHave;
	public List<string> NiveToHave;
	public List<string> OutputFlags;
	public List<string> MustNotHave;

	public Dictionary<string, Productionist> Productionists = new Dictionary<string, Productionist>();
	public Productionist ActiveProductionist { get; set; }

	void Awake ()
	{
		if (instance == null)
			instance = this;

		else if (instance != this)
			Destroy(gameObject);
		
		foreach (string file in System.IO.Directory.GetFiles(@Application.dataPath + "/Grammars"))
		{
			if (file.Contains(".meta") || !file.Contains(".json"))
			{
				continue;
			}
			string frontDelimiter = "Grammars";
			string backDelimiter = ".json";
			int startCut = file.IndexOf(frontDelimiter) + frontDelimiter.Length + 1;
			string grammar = file.Substring(startCut, file.Length - (startCut + backDelimiter.Length));
			Productionists.Add(grammar, new Productionist(grammar));
		}
	}

}

