﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

public class Turn
{
	public CharacterState CharacterState;
	public AgricultureState AgricultureState;
	public CommerceState CommerceState;
	public PublicServiceState PublicServiceState;
	public CrisesState CrisesState;
	public RatsState RatsState;
	public DialogState DialogState;
	public GlobalState GlobalState;

	public Turn(CharacterState chaS, AgricultureState agrS, CommerceState comS, PublicServiceState pubS, CrisesState criS, RatsState ratS, DialogState diaS, GlobalState gloS)
	{
		CharacterState = chaS;
		AgricultureState = agrS;
		CommerceState = comS;
		PublicServiceState = pubS;
		CrisesState = criS;
		RatsState = ratS;
		DialogState = diaS;
		GlobalState = gloS;
	}

    public void Log()
    {
        string filePath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments) +
                          "/HammurabiLogs/";
        System.IO.Directory.CreateDirectory(filePath);

        string fileName = GameStateManager.instance.startTime+".txt";

        // Output game info to text file
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("********************************************");
        sb.AppendLine("                TURN " + GameStateManager.instance.TurnNumber);
        sb.AppendLine("********************************************");
        foreach (State state in GameStateManager.instance.States)
        {
            sb.AppendLine(state.ToString());
        }
        sb.AppendLine();
        filePath = System.IO.Path.Combine(filePath, fileName);
        File.AppendAllText(filePath, sb.ToString());
    }
}

public class GameStateManager : MonoBehaviour {
	
	public static GameStateManager instance = null;
	
	public CharacterState CharacterState { get; private set; }
	public AgricultureState AgricultureState { get; private set; }
	public CommerceState CommerceState { get; private set; }
	public PublicServiceState PublicServiceState { get; private set; }
	public CrisesState CrisesState { get; private set; }
	public RatsState RatsState { get; private set; }
	public DialogState DialogState { get; private set; }
	public GlobalState GlobalState { get; private set; }

	public List<State> States { get; private set; }
	public List<Turn> Turns { get; private set; }

    public String startTime { get; private set; }

	public int TurnNumber { get; private set; }

	public Turn LastTurn
	{
		get	{ return TurnNumber > 0 ? Turns[TurnNumber - 1] : null; }
	}

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
		}

		DontDestroyOnLoad(gameObject);

		CharacterState = new CharacterState();
		AgricultureState = new AgricultureState();
		CommerceState = new CommerceState();
		PublicServiceState = new PublicServiceState();
		CrisesState = new CrisesState();
		RatsState = new RatsState();
		DialogState = new DialogState();
		GlobalState = new GlobalState();
	}

	void Start()
	{
		States = new List<State>();
		States.Add(CharacterState);
		States.Add(AgricultureState);
		States.Add(CommerceState);
		States.Add(PublicServiceState);
		States.Add(CrisesState);
		States.Add(RatsState);
		States.Add(DialogState);
		States.Add(GlobalState);

		Turns = new List<Turn>();
		TurnNumber = 0;

	    DateTime now = DateTime.Now;
		startTime = ((now.Month<10) ? "0" : "") + now.Month+"-"+
			((now.Day<10) ? "0" : "") + now.Day+"-"+
			now.Year+"--"+
			((now.Hour<10) ? "0" : "") + now.Hour+"-"+
			((now.Minute<10) ? "0" : "") + now.Minute+"-"+
			((now.Second<10) ? "0" : "") + now.Second;
	}

	public void AdvanceTurn()
	{
		foreach (State state in States)
		{
			state.AdvanceState();
		}

		string debugTurnStateStr = String.Format("Turn {0}\n", TurnNumber);
		foreach (State state in States)
		{
			debugTurnStateStr += state + "\n";
		}
	    
		AddNewTurn();
	    Turns[TurnNumber].Log();
        Debug.Log(debugTurnStateStr) ;
		TurnNumber++;
	}

	private void AddNewTurn()
	{
		Turns.Add(new Turn(
			new CharacterState(CharacterState),
			new AgricultureState(AgricultureState),
			new CommerceState(CommerceState),
			new PublicServiceState(PublicServiceState),
			new CrisesState(CrisesState),
			new RatsState(RatsState),
			new DialogState(DialogState),
			new GlobalState(GlobalState)
		));
	}

}
