﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.Events;

public class BalanceManager : MonoBehaviour {

	public static BalanceManager instance = null;

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
		}
	}
	
	public CharacterBalance Characters;
	public AgricultureBalance Agriculture;
	public CommerceBalance Commerce;
	public PublicServiceBalance PublicService;
	public CrisesBalance Crises;
	public RatsBalance Rats;
	public GlobalBalance Global;
    public ThresholdBalance Thresholds;
}

[Serializable]
public struct CharacterBalance
{
	[Header("Advisor 1")]
	public string Advisor1Name;
	public string Advisor1IntroductionGrammar;
	public string Advisor1AdviceGrammar;
	public string Advisor1DecisionGrammar;
	public string Advisor1ReportGrammar;
	public int Advisor1Approval;
	public int Advisor1Grain;
	public int Advisor1Land;

	[Header("Advisor 2")]
	public string Advisor2Name;
	public string Advisor2IntroductionGrammar;
	public string Advisor2AdviceGrammar;
	public string Advisor2DecisionGrammar;
	public string Advisor2ReportGrammar;
	public int Advisor2Approval;
	public int Advisor2Grain;
	public int Advisor2Land;

	[Header("Advisor 3")]
	public string Advisor3Name;
	public string Advisor3IntroductionGrammar;
	public string Advisor3AdviceGrammar;
	public string Advisor3DecisionGrammar;
	public string Advisor3ReportGrammar;
	public int Advisor3Approval;
	public int Advisor3Grain;
	public int Advisor3Land;

	[Header("Markup Thresholds")]
	public int ApprovalThreshold;
	public int GrainThreshold;
	public int LandThreshold;
}

[Serializable]
public struct AgricultureBalance
{
	[Header("Production Modifier")]
	public int ProductionModifierLow;
	public int ProductionModifierHigh;

	[Header("Land Worked")]
	public int LandWorkedByPopulation;

	[Header("Tampering and Competence")]
	public float StealPercentage;
	public float GivePercentage;
	public int CompetenceModifier;
}

[Serializable]
public struct CommerceBalance
{
	[Header("Price of Land")]
	public int PriceOfLandRndLow;
	public int PriceOfLandRndHigh;
	public int PriceOfLandModifier;

	[Header("Tampering and Competence")]
	public float GrainStealPercentage;
	public float GrainGivePercentage;
	public float LandStealPercentage;
	public float LandGivePercentage;
	public int CompetenceModifier;
}

[Serializable]
public struct PublicServiceBalance
{
	[Header("Feeding")]
	public int GrainToFeedPopulationUnit;
	public float OverfeedThreshold;

	[Header("Immigration")]
	public int ImmigrationRndLow;
	public int ImmigrationRndHigh;
	public int ImmigrationDiviser;

	[Header("Approval and Happiness")]
	public int BaseApprovalIncrement;
	public int AdvancedApprovalIncrement;
	public int ApprovalDecrement;
	public int HappinessDecrement;
	public int HappinessIncrement;

	[Header("Tampering and Competence")]
	public float StealPercentage;
	public float GivePercentage;
	public int CompetenceModifier;
}

[Serializable]
public struct CrisesBalance
{
	[Header("General")]
	public int RngMultiplier;
	public int ThresholdMultiplier;
	public int HappinessDecrement;

	[Header("Timer")]
	public int BaseTimerIncrement;
	public int TimerIncrementMutliplier;
	public int TimerRndLow;
	public int TimerRndHigh;

	[Header("Earthquake")]
	public int EarthquakeBaseThreshold;
	public float EarthquakeLandModifier;
	public float EarthquakePopulationModifier;

	[Header("Locust")]
	public int LocustBaseThreshold;
	public float LocustGrainModifier;
	
	[Header("Plague")]
	public int PlagueBaseThreshold;
	public float PlaguePopulationModifier;
}

[Serializable]
public struct RatsBalance
{
	[Header("RNG")]
	public int RndLow;
	public int RndHigh;

	[Header("level1")]
	public int Threshold1;
	public float Modifier1;

	[Header("level2")]
	public int Threshold2;
	public float Modifier2;

	[Header("level3")]
	public int Threshold3;
	public float Modifier3;

	[Header("level4")]
	public int Threshold4;
	public float Modifier4;
}

[Serializable]
public struct GlobalBalance
{
	[Header("Global")]
	public int BaseAcreage;
	public int BaseOtherLand;
	public int BaseGrainStore;
	public int BasePopulation;
	public int BaseHappiness;
	public int BaseKingApproval;
}

[Serializable]
public struct ThresholdBalance
{
	public int Immigration; //TODO: Confirm with designers and remove this (unused)?
    public int Starvations_lower;
	public int Starvations_upper;
    public int HarvestYield_lower;
	public int HarvestYield_upper;
    public int ProdModifier_lower;
	public int ProdModifier_upper;
    public float Overfeed;
    public int Rats;
    public int LandSold_lower;
	public int LandSold_upper;
    public int LandBought_lower;
	public int LandBought_upper;
    public int KingApproval_lower;
	public int KingApproval_upper;
    public int KingLand_lower;
	public int KingLand_upper;
    public int KingGrain_lower;
	public int KingGrain_upper;
	public int AdvisorApproval_upper;
	public int AdvisorApproval_lower;
	public int AdvisorLand_lower;
	public int AdvisorLand_upper;
	public int AdvisorGrain_lower;
	public int AdvisorGrain_upper;
	public float PlantedRatio_lower;
	public float PlantedRatio_upper;

    // Values for advice grammar
    public int Population_Upper;
    public int Population_Lower;

    public int LandPrice_upper;
    public int LandPrice_lower;

    public int Workers_upper;
    public int Workers_lower;

}
