﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class DemoPanel : MonoBehaviour
{
	private const string kAssignment = "Assignment";
	private const string kGlobalState = "GlobalState";
	private const string kAgricultureState = "AgricultureState";
	private const string kCommerceState = "CommerceState";
	private const string kPublicServiceState = "PublicServiceState";
	private const string kAdvisorState = "AdvisorState";
	private const string kAdvisorMood = "AdvisorMood";
	private const string kOutputFlags = "OutputFlags";
	private const string kStructure = "Structure";
	private const string kNone = "None";

	public enum AdvisorGrammar
	{
        DemoChris,
		DumbOaf_v3,
		Sansa_v10_t3
	}

	public AdvisorGrammar Advisor1Grammar;
	public AdvisorGrammar Advisor2Grammar;
	public AdvisorGrammar Advisor3Grammar;

	public Text MarkupText;
	public Button GenerateExpressionButton;
	public Text Advisor1Name;
	public Text Advisor2Name;
	public Text Advisor3Name;
	public Text Advisor1Output;
	public Text Advisor2Output;
	public Text Advisor3Output;
	
	void Start()
	{
		Advisor1Name.text = Advisor1Grammar.ToString();
		Advisor2Name.text = Advisor2Grammar.ToString();
		Advisor3Name.text = Advisor3Grammar.ToString();
	}

	public string GenerateMarkup()
	{
		string returnedMarkup = "Agriculture";
		Dictionary<string, string[]> markupSets = DialogManager.instance.Productionists[Advisor2Grammar.ToString()].MarkupSets;

		foreach (string tagset in markupSets.Keys)
		{
			if(tagset != "AgricultureState")
			{
				continue;
			}

			switch (tagset)
			{
				case kAssignment:
				case kCommerceState:
				case kPublicServiceState:
				case kOutputFlags:
				case kNone:
					continue;
				default:

					Dictionary<string, List<string>> subTagSets = new Dictionary<string, List<string>>();

					foreach (string tag in markupSets[tagset])
					{
						string key = tag.Split('_')[0];
						string value = tag.Split('_')[1];

						if (!subTagSets.ContainsKey(key))
						{
							subTagSets.Add(key, new List<string>());
						}

						subTagSets[key].Add(value);
					}
					
					foreach (string subTagSet in subTagSets.Keys)
					{
						List<string> valuePool = new List<string>();

						foreach (string subTag in subTagSets[subTagSet])
						{
							valuePool.Add(subTagSet + "_" + subTag);
						}

						string potentialMarkup = valuePool[UnityEngine.Random.Range(0, valuePool.Count)];
						returnedMarkup += ";" + potentialMarkup;
					}
					
					break;
			}
		}
		
		return returnedMarkup;
	}

	public void UI_GenerateExpression()
	{
		bool impossibleExpressions = true;

		string markup = "";
		Expression adv1Output = null;
		Expression adv2Output = null;
		Expression adv3Output = null;

		while (impossibleExpressions)
		{
			markup = GenerateMarkup();

			adv1Output = DialogManager.instance.Productionists[Advisor1Grammar.ToString()].GenerateExpression(markup);
			if(adv1Output.Line.Contains("Impossible"))
			{
				continue;
			}
			adv2Output = DialogManager.instance.Productionists[Advisor2Grammar.ToString()].GenerateExpression(markup);
			if (adv2Output.Line.Contains("Impossible"))
			{
				continue;
			}
			adv3Output = DialogManager.instance.Productionists[Advisor3Grammar.ToString()].GenerateExpression(markup);
			if (adv3Output.Line.Contains("Impossible"))
			{
				continue;
			}

			impossibleExpressions = false;
		}
		
		Advisor1Output.text = adv1Output.Line;
		Advisor2Output.text = adv2Output.Line;
		Advisor3Output.text = adv3Output.Line;

		markup = markup.Replace(";", " ; ");
		MarkupText.text = markup;
		Debug.Log(markup);
	}
}
