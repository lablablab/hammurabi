﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.EventSystems;

public class TurnPanel : MonoBehaviour {

	public Text TurnText;
	public Text GrainText;
	public Text LandText;
	public Text PopulationText;

	public Dropdown AgricultureAssignment;
	public Dropdown CommerceAssignment;
	public Dropdown PublicServiceAssignment;
	public Dropdown CommerceAction;
	public InputField AgricultureBudget;
	public InputField CommerceBudget;
	public InputField PublicServiceBudget;

	public Text AgricultureAdvisor;
	public Text CommerceAdvisor;
	public Text PublicServiceAdvisor;

	public InputField AgrAdvGrainTransfer;
	public InputField ComAdvGrainTransfer;
	public InputField PubAdvGrainTransfer;
	public InputField AgrAdvLandTransfer;
	public InputField ComAdvLandTransfer;
	public InputField PubAdvLandTransfer;

	public Button AgricultureReportButton;
	public Button CommerceReportButton;
	public Button PublicServiceReportButton;
	public Button AgricultureAdviceButton;
	public Button CommerceAdviceButton;
	public Button PublicServiceAdviceButton;

	public Button AssignmentConfirmationButton;
	public Button NextTurnButton;

	private GameStateManager _gameState;
	private int _totalGrainBudget;
	private int _totalLandBudget;
	private bool _assignmentsConfirmed;
	
	public static TurnPanel instance = null;

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
		}

		//DontDestroyOnLoad(gameObject);
	}

	void Start()
	{
		_gameState = GameStateManager.instance;

		GrainText.text = String.Format("{0}/{0}", _gameState.GlobalState.GrainStore);
		LandText.text = String.Format("{0}/{0}", _gameState.GlobalState.Acreage);
		PopulationText.text = _gameState.GlobalState.Population.ToString();

		Dropdown[] assignmentDropdowns = GetComponentsInChildren<Dropdown>();
		for (int i = 0; i < assignmentDropdowns.Length; i++)
		{
			for(int j = 0; j < _gameState.CharacterState.Characters.Count; j++)
			{
				if(assignmentDropdowns[i] != CommerceAction)
				{
					assignmentDropdowns[i].options.Add(new Dropdown.OptionData(_gameState.CharacterState.Characters[j].Name));
				}
			}
		}

		ResetInputFields();
	}

	public void ResetDropdowns()
	{
		Dropdown[] assignmentDropdowns = GetComponentsInChildren<Dropdown>();
		for (int i = 0; i < assignmentDropdowns.Length; i++)
		{
			assignmentDropdowns[i].value = 0;
			assignmentDropdowns[i].interactable = true;
		}
	}

	public void ResetInputFields()
	{
		InputField[] inputFields = GetComponentsInChildren<InputField>();
		for (int i = 0; i < inputFields.Length; i++)
		{
			inputFields[i].text = "0";
		}

		_assignmentsConfirmed = false;
	}

	public void UpdateAdviceButtons()
	{
		AgricultureAdviceButton.interactable = _gameState.CharacterState.AgricultureAdvisor.Advice != null;
		CommerceAdviceButton.interactable = _gameState.CharacterState.CommerceAdvisor.Advice != null;
		PublicServiceAdviceButton.interactable = _gameState.CharacterState.PublicServiceAdvisor.Advice != null;
	}

	public void UpdateReportButtons()
	{
		AgricultureReportButton.interactable = _gameState.CharacterState.AgricultureAdvisor.Report != null;
		CommerceReportButton.interactable = _gameState.CharacterState.CommerceAdvisor.Report != null;
		PublicServiceReportButton.interactable = _gameState.CharacterState.PublicServiceAdvisor.Report != null;
	}
	
	public void ComputeTotals()
	{
		_totalLandBudget = 0;
		_totalGrainBudget = 0;

		InputField[] inputFields = GetComponentsInChildren<InputField>();
		for (int i = 0; i < inputFields.Length; i++)
		{
			if (inputFields[i].text == "")
			{
				inputFields[i].text = "0";
			}

			int value = Int32.Parse(inputFields[i].text);

			if (inputFields[i] == AgrAdvLandTransfer ||
				inputFields[i] == ComAdvLandTransfer ||
				inputFields[i] == PubAdvLandTransfer ||
				(CommerceAction.value == 2 && inputFields[i] == CommerceBudget))
			{
				_totalLandBudget += value;
			}
			else
			{
				_totalGrainBudget += value;
			}
		}
	}

	public void UI_ResetInput()
	{
		_assignmentsConfirmed = false;

		foreach (Character character in _gameState.CharacterState.Characters)
		{
			character.Advice = null;
		}

		ResetDropdowns();
		ResetInputFields();
		UI_UpdateTextInfo();

		UpdateAdviceButtons();
		UpdateReportButtons();
	}
	
	public void UI_UpdateOfficeAssignments()
	{
		AgricultureAdvisor.text = AgricultureAssignment.captionText.text;
		CommerceAdvisor.text = CommerceAssignment.captionText.text;
		PublicServiceAdvisor.text = PublicServiceAssignment.captionText.text;
	}
	
	public void UI_UpdateTextInfo()
	{
		ComputeTotals();

		Color normalGrey = new Color(0.2f, 0.2f, 0.2f);

		GrainText.text = String.Format("{0}/{1}", _gameState.GlobalState.GrainStore - _totalGrainBudget, _gameState.GlobalState.GrainStore);
		GrainText.color = _gameState.GlobalState.GrainStore - _totalGrainBudget >= 0 ? normalGrey : Color.red;
		LandText.text = String.Format("{0}/{1}", _gameState.GlobalState.Acreage - _totalLandBudget, _gameState.GlobalState.Acreage);
		LandText.color = _gameState.GlobalState.Acreage - _totalLandBudget >= 0 ? normalGrey : Color.red;
	}

	public void UI_UpdateCommerceAction()
	{
		Color grainYellow = new Color(0.97f, 0.91f, 0.43f);
		Color landGreen = new Color(0.67f, 0.87f, 0.40f);

		switch(CommerceAction.value)
		{
			case 1:
				CommerceBudget.GetComponent<Image>().color = grainYellow;
				CommerceBudget.interactable = true;
				break;
			case 2:
				CommerceBudget.GetComponent<Image>().color = landGreen;
				CommerceBudget.interactable = true;
				break;
			default:
				CommerceBudget.GetComponent<Image>().color = Color.white;
				CommerceBudget.text = "0";
				CommerceBudget.interactable = false;
				break;
		}
	}

	public void UI_AdvanceValidation()
	{
		Dropdown[] dropdowns = GetComponentsInChildren<Dropdown>();
		for (int i = 0; i < dropdowns.Length; i++)
		{
			if(dropdowns[i] == CommerceAction)
			{
				continue;
			}

			if (dropdowns[i].value == 0)
			{
				NextTurnButton.interactable = false;
				AssignmentConfirmationButton.interactable = false;
				return;
			}

			for (int j = i; j < dropdowns.Length; j++)
			{
				if (dropdowns[j] == CommerceAction)
				{
					continue;
				}

				if (dropdowns[i].value == dropdowns[j].value && i != j)
				{
					NextTurnButton.interactable = false;
					AssignmentConfirmationButton.interactable = false;
					return;
				}
			}
		}
		ComputeTotals();

		AssignmentConfirmationButton.interactable = !_assignmentsConfirmed;

		NextTurnButton.interactable = _totalLandBudget <= _gameState.GlobalState.Acreage && _totalGrainBudget <= _gameState.GlobalState.GrainStore && _assignmentsConfirmed;
	}

	public void UI_ConfirmAssignments()
	{
		_gameState.CharacterState.AgricultureAdvisor = _gameState.CharacterState.Characters[AgricultureAssignment.value - 1];
		_gameState.CharacterState.CommerceAdvisor = _gameState.CharacterState.Characters[CommerceAssignment.value - 1];
		_gameState.CharacterState.PublicServiceAdvisor = _gameState.CharacterState.Characters[PublicServiceAssignment.value - 1];

		AgricultureAssignment.interactable = false;
		CommerceAssignment.interactable = false;
		PublicServiceAssignment.interactable = false;

		AssignmentConfirmationButton.interactable = false;

	    string adviceMarkup = _gameState.DialogState.GenerateAdviceMarkup() + ";" + 
            _gameState.DialogState.GenerateReportMarkup();

	    _gameState.CharacterState.AgricultureAdvisor.Advice = 
            _gameState.CharacterState.AgricultureAdvisor.GenerateAdvice("Assignment_Agriculture;" + adviceMarkup);
		
		_gameState.CharacterState.CommerceAdvisor.Advice=
			_gameState.CharacterState.CommerceAdvisor.GenerateAdvice ("Assignment_Commerce;" + adviceMarkup);

        _gameState.CharacterState.PublicServiceAdvisor.Advice=
			_gameState.CharacterState.PublicServiceAdvisor.GenerateAdvice("Assignment_PublicService;" + adviceMarkup +
            ";" );
		//foreach(Character character in _gameState.CharacterState.Characters)
		//{
		//	character.Advice = character.AdviceGrammar.GenerateExpression();
		//}

		UpdateAdviceButtons();
		_assignmentsConfirmed = true;
	}

	public void UI_AdvanceTurn()
	{
		_gameState.AdvanceTurn();
		_assignmentsConfirmed = false;

		TurnText.text = _gameState.TurnNumber.ToString();
		GrainText.text = String.Format("{0}/{0}", _gameState.GlobalState.GrainStore);
		LandText.text = String.Format("{0}/{0}", _gameState.GlobalState.Acreage);
		PopulationText.text = _gameState.GlobalState.Population.ToString();

		AgricultureAssignment.interactable = !_assignmentsConfirmed;
		CommerceAssignment.interactable = !_assignmentsConfirmed;
		PublicServiceAssignment.interactable = !_assignmentsConfirmed;

		foreach (Character character in _gameState.CharacterState.Characters)
		{
			character.Advice = null;
		}

		UpdateReportButtons();
		UpdateAdviceButtons();
		ResetInputFields();

		UI_AdvanceValidation();
	}

	public void UI_AutoFill()
	{
		if(!_assignmentsConfirmed)
		{
			Dropdown[] assignmentDropdowns = GetComponentsInChildren<Dropdown>();
			for (int i = 0; i < assignmentDropdowns.Length; i++)
			{
				if (assignmentDropdowns[i] == CommerceAction)
				{
					assignmentDropdowns[i].value = 0;
				}
				else
				{
					assignmentDropdowns[i].value = (i + 1);
				}
			}
		}
		
		// TODO:
		// Figure out a nice balance for automatically filling inputs for testing
		// e.g. feed all population, then put a certain amount of the remaining grain in agriculture, then...

		InputField[] inputFields = GetComponentsInChildren<InputField>();
		for (int i = 0; i < inputFields.Length; i++)
		{
			inputFields[i].text = "0";
		}

		UI_UpdateTextInfo();
	}

	public void UI_DisplayExpression()
	{
		Transform buttonInfo = EventSystem.current.currentSelectedGameObject.transform;

		Character character = _gameState.CharacterState.GetCharacterByOffice(buttonInfo.parent.name);
		string dialogMove = buttonInfo.name.Substring(4);

		switch (dialogMove)
		{
			case "Advice":
				UIManager.instance.ExpressionPanel.DisplayExpression(character.Name, character.Advice.Line);
				break;
			case "Report":
				UIManager.instance.ExpressionPanel.DisplayExpression(character.Name, character.Report.Line);
				break;
		}

		UIManager.instance.ExpressionPanel.gameObject.SetActive(true);
	}
}
