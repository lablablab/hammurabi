﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class JacquesMainPanel : MonoBehaviour {

    public Text HowManyText;
    public InputField HowManyInput;
    public Button GenerateBtn;
	public Transform DialogContainer;
	public Text DialogLinePrefab;
	public Text Button1;
	public Text Button2;
	public Text Button3;
	private string Button1_flags;
	private string Button2_flags;
	private string Button3_flags;

	private Productionist _jacquesGrammar;
	private Productionist _maitreGrammar;
	private Expression _jacquesExpression;
	private Expression _maitreExpression;
	public string _jacquesFlags;
	public string _maitreFlags;

	//variables to track

	public int intro = 1;
	public int amours = 0;
	public int fermiere = 0;
	public int genou = 0;
	public int mari = 0;
	public int chirurgiens = 0;

	void Start()
	{
		_jacquesGrammar = DialogManager.instance.Productionists["jacques"];
		_maitreGrammar = DialogManager.instance.Productionists["maitre"];

		_jacquesFlags = "";
		_maitreFlags = "";

		SetGameStateFlags ();

		JacquesTalk ();
	
	}

	public void SetGameStateFlags()
	{
		_jacquesFlags = _jacquesFlags + ";" + "Intro_" + intro;
		_maitreFlags = _maitreFlags + ";" + "Intro_" + intro;
		_jacquesFlags = _jacquesFlags + ";" + "Amours_" + amours;
		_maitreFlags = _maitreFlags + ";" + "Amours_" + amours;
		_jacquesFlags = _jacquesFlags + ";" + "Fermiere_" + fermiere;
		_maitreFlags = _maitreFlags + ";" + "Fermiere_" + fermiere;
		_jacquesFlags = _jacquesFlags + ";" + "Genou_" + genou;
		_maitreFlags = _maitreFlags + ";" + "Genou_" + genou;
		_jacquesFlags = _jacquesFlags + ";" + "Mari_" + mari;
		_maitreFlags = _maitreFlags + ";" + "Mari_" + mari;
		_jacquesFlags = _jacquesFlags + ";" + "Chirurgiens_" + chirurgiens;
		_maitreFlags = _maitreFlags + ";" + "Chirurgiens_" + chirurgiens;

	}

	public void UpdateGameStateVars(String outputflags)

	{
		Debug.Log ("Update Game State Vars with flags: " + outputflags);
		if(outputflags.Contains("inc_intro"))
		{
			intro++;
		}
		if(outputflags.Contains("inc_amours"))
		{
			amours++;
		}
		if(outputflags.Contains("inc_fermiere"))
		{
			fermiere++;
		}
		if(outputflags.Contains("inc_mari"))
		{
			mari++;
		}
		if(outputflags.Contains("inc_chirurgiens"))
		{
			chirurgiens++;
		}
		if(outputflags.Contains("inc_genou"))
		{
			genou++;
		}

	}
	public void JacquesTalk(){

		_jacquesExpression = null;
		_maitreExpression = null;

		Text newDialogLine = null;

		SetGameStateFlags ();
		_jacquesExpression = _jacquesGrammar.GenerateExpression(_maitreFlags);
		_jacquesFlags = _jacquesGrammar.OutputFlags;

		newDialogLine = Instantiate(DialogLinePrefab, DialogContainer, false);
		newDialogLine.text = "Jacques: " + _jacquesExpression.Line;

		UpdateGameStateVars (_jacquesFlags);


		FillButtons ();
		GameObject.Find ("Output").GetComponent<ScrollRect> ().velocity = new Vector2 (0f, 1000f);
		//GameObject.Find ("scrollbar").GetComponent<Scrollbar> ().value *= 0.1f;
		//Debug.Log (GameObject.Find ("Scrollbar Vertical").GetComponent<Scrollbar> ().value);//= 0;
		Canvas.ForceUpdateCanvases ();
	}
	public void FillButtons()
    {

		_jacquesExpression = null;
		_maitreExpression = null;

		Text newDialogLine = null;


		SetGameStateFlags ();

		Button1.text = _maitreGrammar.GenerateExpression(_jacquesFlags).Line;
		Button1_flags = _maitreGrammar.OutputFlags;

		Button2.text = _maitreGrammar.GenerateExpression (_jacquesFlags).Line;
		Button2_flags = _maitreGrammar.OutputFlags;

		int testnum = 0;
		while (Button2.text == Button1.text) {
			Button2.text = _maitreGrammar.GenerateExpression (_jacquesFlags).Line;
			Button2_flags = _maitreGrammar.OutputFlags;
			testnum++;
			if (testnum > 100)
				break;
		}

		Button3.text = _maitreGrammar.GenerateExpression (_jacquesFlags).Line;
		Button3_flags = _maitreGrammar.OutputFlags;

		testnum = 0;
		while ((Button3.text == Button2.text) || (Button3.text == Button1.text)) {
			Button3.text = _maitreGrammar.GenerateExpression (_jacquesFlags).Line;
			Button3_flags = _maitreGrammar.OutputFlags;
			testnum++;
			if (testnum > 100)
				break;
		}

    }

	public void Button1Click(){
		Text newDialogLine = Instantiate(DialogLinePrefab, DialogContainer, false);
		newDialogLine.text = "Maitre: " + Button1.text;
		_maitreFlags = Button1_flags;
		UpdateGameStateVars (Button1_flags);
		JacquesTalk ();
	}
	public void Button2Click(){
		Text newDialogLine = Instantiate(DialogLinePrefab, DialogContainer, false);
		newDialogLine.text = "Maitre: " + Button2.text;		
		_maitreFlags = Button2_flags;
		UpdateGameStateVars (Button2_flags);
		JacquesTalk ();
	}
	public void Button3Click(){
		Text newDialogLine = Instantiate(DialogLinePrefab, DialogContainer, false);
		newDialogLine.text = "Maitre: " + Button3.text;		
		_maitreFlags = Button3_flags;
		UpdateGameStateVars (Button3_flags);
		JacquesTalk ();
	}

}


