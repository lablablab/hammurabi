﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ExpressionPanel : MonoBehaviour {

	public Text SpeakerText;
	public Text ExpressionText;

	public static ExpressionPanel instance = null;

	void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != this)
		{
			Destroy(gameObject);
		}

		//DontDestroyOnLoad(gameObject);
	}

	public void DisplayExpression(string speaker, string expression)
	{
		SpeakerText.text = speaker;
		ExpressionText.text = expression;
	}

	public void UI_Close()
	{
		gameObject.SetActive(false);
	}
}
