﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

public class ProductionistTestPanel : MonoBehaviour
{
	private const string kAssignment = "Assignment";
	private const string kGlobalState = "GlobalState";
	private const string kAgricultureState = "AgricultureState";
	private const string kCommerceState = "CommerceState";
	private const string kPublicServiceState = "PublicServiceState";
	private const string kAdvisorState = "AdvisorState";
	private const string kAdvisorMood = "AdvisorMood";
	private const string kOutputFlags = "OutputFlags";
	private const string kStructure = "Structure";
	private const string kNone = "None";

	public class MarkupDropdown
	{
		public string Key;
		public Dropdown Dropdown;

		public string Value
		{
			get { return Dropdown.GetComponentInChildren<Text>().text; }
		}

		public MarkupDropdown(string key, Dropdown dropdown)
		{
			Key = key;
			Dropdown = dropdown;
		}
	}

	/*public class MarkupToggle
	{
		public string Key;
		public Toggle Toggle;

		public bool Value
		{
			get { return Toggle.isOn; }
		}

		public MarkupToggle(string key, Toggle toggle)
		{
			Key = key;
			Toggle = toggle;
		}
	}*/

	public class MarkupInput
	{
		public string Key;
		public InputField Input;

		public string Value
		{
			get { return Input.text; }
		}

		public MarkupInput(string key, InputField input)
		{
			Key = key;
			Input = input;
		}
	}

	public Dropdown GrammarDropdown;
	public Dropdown DialogMoveDropdown;
	public Transform SystemVariables;
	public Transform OfficeState;
	public Transform GlobalState;
	public Transform AdvisorMood;
	public Transform AdvisorState;
	public Button GenerateTagsetsButton;

	public Text OutputText;
	public Text OutputFlags;

	public GameObject MarkupDropdownPrefab;
	public GameObject MarkupTogglePrefab;
	public GameObject MarkupInputPrefab;

	private List<MarkupDropdown> MarkupDropdowns = new List<MarkupDropdown>();
	//private List<MarkupToggle> MarkupToggles = new List<MarkupToggle>();
	public List<MarkupInput> MarkupInputs = new List<MarkupInput>();

	private Productionist Productionist
	{
		get	{ return DialogManager.instance.ActiveProductionist; }
		set	{ DialogManager.instance.ActiveProductionist = value; }
	}

	private string _markup
	{
		get
		{
			string returnedMarkup = "";
			foreach (MarkupDropdown markup in MarkupDropdowns)
			{
				if (markup.Value != "Any")
				{
					returnedMarkup += (returnedMarkup.Length != 0 ? ";" : "") + (markup.Key != kAssignment ? markup.Key + "_" : "") + markup.Value;
				}
			}
			foreach (MarkupInput markup in MarkupInputs)
			{
				if (markup.Value != "")
				{
					returnedMarkup += (returnedMarkup.Length != 0 ? ";" : "") + markup.Key + "_" + markup.Value;
				}
			}
			return returnedMarkup;
		}
	}

	void Start()
	{
		foreach (string grammar in DialogManager.instance.Productionists.Keys)
		{
			GrammarDropdown.options.Add(new Dropdown.OptionData(grammar));
		}
		
		GrammarDropdown.GetComponentInChildren<Text>().text = GrammarDropdown.options[0].text;
	}

	public void ResetTagsets()
	{
		foreach (Transform transform in OfficeState)
		{
			Destroy(transform.gameObject);
		}

		foreach (Transform transform in GlobalState)
		{
			Destroy(transform.gameObject);
		}

		foreach (Transform transform in AdvisorMood)
		{
			Destroy(transform.gameObject);
		}

		foreach (Transform transform in AdvisorState)
		{
			Destroy(transform.gameObject);
		}

		foreach (Transform transform in SystemVariables)
		{
			Destroy(transform.gameObject);
		}

		MarkupDropdowns.Clear();
		MarkupInputs.Clear();
	}

	public void UI_InitializeGrammar()
	{
		ResetTagsets();
		DialogMoveDropdown.value = 0;
		DialogMoveDropdown.options.Clear();
		
		if(GrammarDropdown.GetComponentInChildren<Text>().text == "...")
		{
			DialogMoveDropdown.interactable = false;
			return;
		}

		DialogManager.instance.ActiveProductionist = DialogManager.instance.Productionists[GrammarDropdown.GetComponentInChildren<Text>().text];

		DialogMoveDropdown.options.Add(new Dropdown.OptionData("..."));
		foreach (string dialogMove in Productionist.MarkupSets["Assignment"])
		{
			DialogMoveDropdown.options.Add(new Dropdown.OptionData(dialogMove));
		}
		
		DialogMoveDropdown.interactable = true;
	}

	public void UI_EnableTagsetsGeneration()
	{
		ResetTagsets();
		GenerateTagsetsButton.interactable = DialogMoveDropdown.GetComponentInChildren<Text>().text != "...";
		MarkupDropdowns.Add(new MarkupDropdown("Assignment", DialogMoveDropdown));
	}

	public void UI_GenerateTagsets()
	{
		UI_EnableTagsetsGeneration();

		foreach (string systemVariable in Productionist.SystemVariables)
		{
			char[] t = { '[', ']' };
			string systemVariableLabel = systemVariable.Trim(t);

			GameObject newInput = Instantiate(MarkupInputPrefab);
			InputField newMarkupInput = newInput.GetComponentInChildren<InputField>();
			newInput.transform.SetParent(SystemVariables, false);
			MarkupInputs.Add(new MarkupInput(systemVariableLabel, newMarkupInput));

			string markupKeyLabel = char.ToUpper(systemVariableLabel[0]) + systemVariableLabel.Substring(1);
			newInput.transform.GetComponentInChildren<Text>().text = markupKeyLabel;
		}

		foreach (string tagset in Productionist.MarkupSets.Keys)
		{
			Transform tagsetContainer = null;

			switch (tagset)
			{
				case kAgricultureState:
					if(DialogMoveDropdown.GetComponentInChildren<Text>().text != "Assignment_Agriculture") 
					{
						continue;
					}
					tagsetContainer = OfficeState;
					break;
				case kCommerceState:
					if (DialogMoveDropdown.GetComponentInChildren<Text>().text != "Assignment_Commerce")
					{
						continue;
					}
					tagsetContainer = OfficeState;
					break;
				case kPublicServiceState:
					if (DialogMoveDropdown.GetComponentInChildren<Text>().text != "Assignment_PublicService")
					{
						continue;
					}
					tagsetContainer = OfficeState;
					break;
				case kGlobalState:
					tagsetContainer = GlobalState;
					break;
				case kAdvisorMood:
					tagsetContainer = AdvisorMood;
					break;
				case kAdvisorState:
					tagsetContainer = AdvisorState;
					break;
				case kAssignment:
				case kOutputFlags:
				case kNone:
					continue;
			}

			Array.Sort(Productionist.MarkupSets[tagset]);

			foreach (string tag in Productionist.MarkupSets[tagset])
			{
				switch(tagset)
				{
					case kAssignment:
					case kOutputFlags:
					case kStructure:
						continue;
				}
				
				string splitKey = tag.Split('_')[0];
				string splitValue = tag.Split('_')[1];

				bool newDropdown = true;
				int oldDropdownIndex = 0;

				for (int i = 0; i < MarkupDropdowns.Count; i++)
				{
					if (MarkupDropdowns[i].Key.ToLower() == splitKey.ToLower())
					{
						newDropdown = false;
						oldDropdownIndex = i;
						break;
					}
				}

				if (newDropdown)
				{
					GameObject newMarkup = Instantiate(MarkupDropdownPrefab);
					Dropdown newMarkupDropdown = newMarkup.GetComponentInChildren<Dropdown>();
					newMarkup.transform.SetParent(tagsetContainer, false);
					MarkupDropdowns.Add(new MarkupDropdown(splitKey, newMarkupDropdown));

					string markupKeyLabel = char.ToUpper(splitKey[0]) + splitKey.Substring(1);
					newMarkup.transform.GetComponentInChildren<Text>().text = markupKeyLabel;

					newMarkupDropdown.options.Add(new Dropdown.OptionData("Any"));

					string markupValueLabel = char.ToUpper(splitValue[0]) + splitValue.Substring(1);
					newMarkupDropdown.options.Add(new Dropdown.OptionData(markupValueLabel));
				}
				else
				{
					string markupValueLabel = char.ToUpper(splitValue[0]) + splitValue.Substring(1);
					MarkupDropdowns[oldDropdownIndex].Dropdown.options.Add(new Dropdown.OptionData(markupValueLabel));
				}
			}
		}
	}

	public void UI_GenerateExpression()
	{
		Debug.Log ("markup: " + _markup);
		OutputText.text = Productionist.GenerateExpression(_markup).Line;
		OutputFlags.text = Productionist.OutputFlags.Length != 0 ? Productionist.OutputFlags : "...";
	}

	public void UI_ResetMarkup()
	{
		foreach (Dropdown dropdown in OfficeState.GetComponentsInChildren<Dropdown>())
		{
			dropdown.value = 0;
		}
		foreach (Dropdown dropdown in GlobalState.GetComponentsInChildren<Dropdown>())
		{
			dropdown.value = 0;
		}
		foreach (Dropdown dropdown in AdvisorMood.GetComponentsInChildren<Dropdown>())
		{
			dropdown.value = 0;
		}
		foreach (Dropdown dropdown in AdvisorState.GetComponentsInChildren<Dropdown>())
		{
			dropdown.value = 0;
		}
		foreach (InputField input in SystemVariables.GetComponentsInChildren<InputField>())
		{
			input.text = "";
		}
	}
}
